import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LecteurComponent } from './Components/lecteur/lecteur.component';

@NgModule({
  declarations: [LecteurComponent],
  imports: [
    CommonModule
  ],
  exports: [
    LecteurComponent
  ]
})
export class SharedModule { }
